from web import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(50), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)
    is_active = db.Column(db.Boolean, default=True)

    def get_id(self):
        return self.id

    @property
    def is_authenticated(self):
        return self.is_active

    @property
    def is_anonymous(self):
        return False


class Employee(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(20))
    last_name = db.Column(db.String(20))
    address = db.Column(db.String(40))
    salary = db.Column(db.Integer)
    birthdate = db.Column(db.Date)
    ssn = db.Column(db.String(40))
