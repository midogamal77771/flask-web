from flask import render_template, redirect, flash, url_for
from flask_login import login_user, current_user, login_required, logout_user

from web import app, db, get_csv_data
from web.models import User, Employee
from web.forms import RegisterForm, LoginForm, SearchForm


@app.route("/")
@app.route("/home")
@app.route("/homepage")
def homepage():
    return render_template('index.html')


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('homepage'))
    form = RegisterForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data, password=form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('User accounts has been created successfully !!', 'success')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register Now', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('homepage'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and user.password == form.password.data:
            flash('Login is completed successfully !!', 'success')
            login_user(user, remember=True)
            return redirect(url_for('homepage'))
        flash('Invalid email or password, try again', 'danger')
    return render_template('login.html', title='Login Now', form=form)


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('homepage'))


@app.route('/search', methods=['GET', 'POST'])
@login_required
def search():
    data = None
    form = SearchForm()
    if form.validate_on_submit():
        employee = Employee.query.filter_by(ssn=form.ssn.data).first()
        if employee:
            data = employee
        else:
            flash('Invalid SNN. no one matches the code', 'danger')
    return render_template('search.html',  title='Employee Search', form=form, data=data)
