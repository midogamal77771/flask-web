import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, login_manager


def get_csv_data(filename):
    import datetime
    results = []
    file = open(filename, 'r')
    for row in file.readlines():
        fields = row.split()

        # Special cases
        if len(fields) == 12:
            fields[6] = ' '.join(fields[6: 8])
            del fields[7]
        fields[5] = fields[5].replace('"', '')
        fields[6] = fields[6].replace('"', '')

        y, m, d = fields[4].split('-')

        # Appending results
        results.append({
            'first_name': fields[0],
            'last_name': fields[2],
            'salary': fields[8],
            'address': fields[6],
            'birth_date':  datetime.datetime(int(y), int(m), int(d)),
            'ssn': fields[3]
        })

    return results


app = Flask(__name__)
app.config.update({
    'SECRET_KEY': 'd25f7a9f987b3551b946dd40bdb209fbb1e07d1c',
    'SQLALCHEMY_TRACK_MODIFICATIONS': True,
    'SQLALCHEMY_DATABASE_URI': 'sqlite:///site.db',
})

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'


class CustomSQLAlchemy(SQLAlchemy):
    def create_all(self):
        super().create_all()
        from web.models import Employee
        for employee in get_csv_data(os.getcwd()+'\web\EMPLOYEE.csv'):
            db_employee = Employee(
                first_name=employee.get('first_name'),
                last_name=employee.get('last_name'),
                salary=employee.get('salary'),
                address=employee.get('address'),
                birthdate=employee.get('birth_date'),
                ssn=employee.get('ssn')
            )
            db.session.add(db_employee)
        db.session.commit()


db = CustomSQLAlchemy(app)


@login_manager.user_loader
def load_user(user_id):
    from web.models import User
    return User.query.filter_by(id=user_id).first()


from web import routs
