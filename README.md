# Flask Web


## Setup Guid

- First install required packages:
    ```
    pip install -r requirements.txt 
    ```

- Second open cmd in the same directory of run.py file, then run the following commands:
   ```
    python
    from web import db
    db.drop_all()
    db.create_all()
    from web.models import Employee
    Employee.query.all()
    exit()
   ```
- Last:
    ```
    python run.py
    ```
